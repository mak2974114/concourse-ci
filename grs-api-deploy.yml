# -*- coding: utf-8 -*-
# Copyright © 2021 VMware, Inc.  All rights reserved.
# VMware Confidential
#
# Pipeline for GoldRush merge requests.
#
---
resource_types:
  - name: gitlab
    type: registry-image
    source:
      repository:
        devtools-docker.artifactory.eng.vmware.com/vmware/runway/resourcetypes/gitlab-resource
      tag: "2.1.7"
  - name: slack-notification
    type: docker-image
    source:
      repository:
        harbor-repo.vmware.com/dockerhub-proxy-cache/cfcommunity/slack-notification-resource
      tag: "1.6.0"

resources:
  - name: source-code
    type: git
    source:
      uri:
        git@gitlab.eng.vmware.com:VMware-Intelligence-Analytics-Platform/grs.git
      branch: master
      private_key: ((GIT_GRS_DEPLOY_KEY))

  - name: slack-alert
    type: slack-notification
    source:
      url:
        https://hooks.slack.com/services/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

jobs:
  - name: deploy_staging
    serial: true
    plan:
      - get: source-code
      - task: deploy
        config:
          platform: linux
          image_resource:
            type: docker-image
            source:
              repository:
                goldrush-docker-local.artifactory.eng.vmware.com/grsbuild_centos8_srp
              tag: v2023.12.04
          inputs:
            - name: source-code
          params:
            GIT_GRS_DEPLOY_KEY: ((GIT_GRS_DEPLOY_KEY))
            YAMLCONF_DBNAME: ((PGDB_GRS_STAGE_NAME))
            YAMLCONF_DBUSER: ((PGDB_GRS_STAGE_USER))
            YAMLCONF_DBHOST: ((PGDB_GRS_STAGE_HOST))
            YAMLCONF_DBPASS: ((PGDB_GRS_STAGE_PASSWD))
            YAMLCONF_SERVER_NAME: 'grs-stg-metadata-manager.eng.vmware.com'
            YAMLCONF_PRODUCTION_USER: 'grsadmin'
            YAMLCONF_PRODUCTION_SERVERS: |
              [
                "{PRODUCTION_USER}@grs-stg-metadata-manager.eng.vmware.com",
                "{PRODUCTION_USER}@grs-stg-api-server-1.eng.vmware.com",
                "{PRODUCTION_USER}@grs-stg-api-server-2.eng.vmware.com"
              ]
          run:
            path: source-code/bin/ci/grs-api-deploy.sh
        on_failure:
          put: slack-alert
          inputs: detect
          params:
            text: |
              ❌ Staging server update failed.

              The $BUILD_JOB_NAME job failed for the $BUILD_PIPELINE_NAME
              pipeline.  See the log available at
              ${ATC_EXTERNAL_URL}/teams/${BUILD_TEAM_NAME}/pipelines/${BUILD_PIPELINE_NAME}/jobs/${BUILD_JOB_NAME}/builds/${BUILD_NAME}
        on_success:
          put: slack-alert
          inputs: detect
          params:
            text: |
              ✅ Staging deployment succeeded, visit
              https://grs-stg-metadata-manager.eng.vmware.com/admin

              Log available at
              ${ATC_EXTERNAL_URL}/teams/${BUILD_TEAM_NAME}/pipelines/${BUILD_PIPELINE_NAME}/jobs/${BUILD_JOB_NAME}/builds/${BUILD_NAME}

  - name: deploy_prod_and_dr
    serial: true
    plan:
      - get: source-code
        passed: [deploy_staging]
      - task: deploy
        config:
          platform: linux
          image_resource:
            type: docker-image
            source:
              repository:
                goldrush-docker-local.artifactory.eng.vmware.com/grsbuild_centos8_srp
              tag: v2023.12.04
          inputs:
            - name: source-code
          params:
            GIT_GRS_DEPLOY_KEY: ((GIT_GRS_DEPLOY_KEY))
            YAMLCONF_DBNAME: ((PGDB_GRS_NAME))
            YAMLCONF_DBUSER: ((PGDB_GRS_OWNER_NAME))
            YAMLCONF_DBHOST: ((PGDB_GRS_HOST))
            YAMLCONF_DBPASS: ((PGDB_GRS_OWNER_PASSWD))
            YAMLCONF_DBPORT: 6543
            YAMLCONF_INSTALL_DIR: /data/grs
            YAMLCONF_SERVER_NAME: 'grs-prd-metadata-manager.eng.vmware.com'
            YAMLCONF_PRODUCTION_USER: 'grsadmin'
            YAMLCONF_PRODUCTION_SERVERS: |
              [
                "{PRODUCTION_USER}@grs-prd-metadata-manager.eng.vmware.com",
                "{PRODUCTION_USER}@grs-prod-api-server-1.eng.vmware.com",
                "{PRODUCTION_USER}@grs-prod-api-server-2.eng.vmware.com",
                "{PRODUCTION_USER}@grs-sc1-dr-metadata-manager.eng.vmware.com",
                "{PRODUCTION_USER}@grs-sc1-dr-api-server-1.eng.vmware.com",
                "{PRODUCTION_USER}@grs-sc1-dr-api-server-2.eng.vmware.com"
              ]
          run:
            path: source-code/bin/ci/grs-api-deploy.sh
        on_failure:
          put: slack-alert
          inputs: detect
          params:
            text: |
              ❌ Production server update failed.

              The $BUILD_JOB_NAME job failed for the $BUILD_PIPELINE_NAME
              pipeline.  See the log available at
              ${ATC_EXTERNAL_URL}/teams/${BUILD_TEAM_NAME}/pipelines/${BUILD_PIPELINE_NAME}/jobs/${BUILD_JOB_NAME}/builds/${BUILD_NAME}
        on_success:
          put: slack-alert
          inputs: detect
          params:
            text: |
              ✅ Production deployment succeeded, visit
              https://grs-prd-metadata-manager.eng.vmware.com/admin
              https://grs-sc1-dr-metadata-manager.eng.vmware.com/admin

              Log available at
              ${ATC_EXTERNAL_URL}/teams/${BUILD_TEAM_NAME}/pipelines/${BUILD_PIPELINE_NAME}/jobs/${BUILD_JOB_NAME}/builds/${BUILD_NAME}
    
